<?php

namespace App\Exceptions;

use Exception;

class SenderWalletNotFoundException extends Exception
{
    //
    public function report(){
        Log::debug("Sender wallet not found!");
    }

}
