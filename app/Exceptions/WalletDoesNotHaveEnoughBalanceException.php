<?php

namespace App\Exceptions;

use Exception;

class WalletDoesNotHaveEnoughBalanceException extends Exception
{
    //
    public function report(){
        Log::debug("Wallet does not have enough balance!");
    }

}
