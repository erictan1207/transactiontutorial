<?php

namespace App\Exceptions;

use Exception;

class CantTransferToSameWalletException extends Exception
{
    //
    public function report(){
        Log::debug("Can not transfer balance between same wallet!");
    }

}
