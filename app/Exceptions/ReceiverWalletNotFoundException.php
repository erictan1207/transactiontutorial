<?php

namespace App\Exceptions;

use Exception;

class ReceiverWalletNotFoundException extends Exception
{
    //
    public function report(){
        Log::debug("Receiver wallet not found!");
    }
}
