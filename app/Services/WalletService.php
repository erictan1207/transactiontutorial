<?php

namespace App\Services;

use App\Jobs\BalanceTransferJob;
use App\Exceptions\CantTransferToSameWalletException;
use App\Exceptions\ReceiverWalletNotFoundException;
use App\Exceptions\SenderWalletNotFoundException;
use App\Exceptions\WalletDoesNotHaveEnoughBalanceException;
use App\Models\Wallet;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class WalletService{

    public function transfer (int $senderWalletId, int $receiverWalletId, float $transferAmount) : array {
        try{

            // BalanceTransferJob::dispatch($senderWalletId, $receiverWalletId, $transferAmount)
            //     ->onQueue('transfer-balance')
            //     ->delay(Carbon::now()->addSeconds(20));

            dispatch(new BalanceTransferJob($senderWalletId, $receiverWalletId, $transferAmount))
                ->onQueue('transfer-balance')
                ->delay(Carbon::now()->addSeconds(20));
           
            return [
                'success' => true,
                'message' => 'Balance transfer request has been submitted, Please wait for your transaction'        
            ];
        }
        catch (\Exception $e){
            DB::rollback();
            return [
                'success' => false,
                'message' => $e->getMessage()        
            ];
        }
    }

    // public function transfer (int $senderWalletId, int $receiverWalletId, float $transferAmount) : array {
    //     try{

    //         DB::beginTransaction();

    //         if($senderWalletId == $receiverWalletId){
    //             throw new CantTransferToSameWalletException ('Can not transfer between same wallet!');
    //         }

    //         $senderWallet = Wallet::where('user_id', $senderWalletId)->first();
    //         if(!$senderWallet){
    //             throw new SenderWalletNotFoundException('Wallet not found!');
    //         }

    //         $senderHasEnoughBalance = $this->hasEnoughBalanceToTransfer($senderWalletId, $transferAmount);
    //         if(!$senderHasEnoughBalance){
    //             throw new WalletDoesNotHaveEnoughBalanceException('Sender does not have enough balance!');
    //         }
            
    //         $senderWallet->decrement('balance', $transferAmount);

    //         $receiverWallet = Wallet::where('user_id', $receiverWalletId)
    //             ->increment('balance', $transferAmount);
    //         if(!$receiverWallet){
    //             throw new ReceiverWalletNotFoundException('Wallet not found!');
    //         }

    //         DB::commit();
    //         return [
    //             'success' => true,
    //             'message' => 'Balance has been transferred'        
    //         ];
    //     }
    //     catch (\Exception $e){
    //         DB::rollback();
    //         return [
    //             'success' => false,
    //             'message' => $e->getMessage()        
    //         ];
    //     }
    // }

    public function hasEnoughBalanceToTransfer (int $walletId, float $transferAmount) : bool {
        $senderWalletBalance = Wallet::where('user_id', $walletId)->first()->balance;
        return $senderWalletBalance >= $transferAmount ? true : false;
    }

    public function transferInADifferentWay (int $senderWalletId, int $receiverWalletId, float $transferAmount) : array {
        try{
            $transferValidationResponse = $this->balanceTransferValidation($senderWalletId, $receiverWalletId, $transferAmount);
            if (!$transferValidationResponse['success']) {
                return [
                    'success' => false,
                    'message' => $transferValidationResponse['message']      
                ];
            }

            DB::beginTransaction();
            
            $senderWallet->decrement('balance', $transferAmount);
            $receiverWallet = Wallet::where('user_id', $receiverWalletId)
                ->increment('balance', $transferAmount);

            DB::commit();
            return [
                'success' => true,
                'message' => 'Balance has been transferred'        
            ];
        }
        catch (\Exception $e){
            DB::rollback();
            return [
                'success' => false,
                'message' => $e->getMessage()        
            ];
        }
    }
   
    public function balanceTransferValidation (int $senderWalletId, int $receiverWalletId, float $transferAmount) : array {

        $exceptionMessages = [];
        $senderWallet = Wallet::where('user_id', $senderWalletId)->first();
        $receiverWallet = Wallet::where('user_id', $receiverWalletId)->first();
        if (isset($senderWallet) && isset($receiverWallet) && $senderWallet->balance >= $transferAmount){
            return [
                'success' => true,
                'message' => 'Balance transfer is possible'        
            ];
        } else {
            if (!isset($senderWallet)) {
                array_push($exceptionMessages, 'Sender wallet not found!');
            } else {
                if ($senderWallet->balance < $transferAmount) {
                    array_push($exceptionMessages, 'Not enough balance in sender wallet!');
                }
            }
            if (!isset($receiverWallet)) {
                array_push($exceptionMessages, 'Receover wallet not found!');
            }
        }

        return [
            'success' => false,
            'message' => $exceptionMessages      
        ];
        


    }

}