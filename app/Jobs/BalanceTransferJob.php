<?php

namespace App\Jobs;

use App\Services\WalletService;
use App\Models\Wallet;
use Illuminate\Support\Facades\DB;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class BalanceTransferJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $senderWalletId;
    protected $receiverWalletId;
    protected $transferAmount;
    protected $walletService;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($senderWalletId, $receiverWalletId, $transferAmount)
    {
        $this->walletService = new WalletService();
        $this->senderWalletId = $senderWalletId;
        $this->receiverWalletId = $receiverWalletId;
        $this->transferAmount = $transferAmount;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            DB::beginTransaction();
            $transferValidationResponse = $this->WalletService->balanceTransferValidation(
                $this->senderWalletId,
                $this->receiverWalletId,
                $this->transferAmount
            );
            if ($transferValidationResponse['success']){
                Wallet::where('user_id',$this->$senderWalletId)->decrement('balance', $this->transferAmount);
                Wallet::where('user_id',$this->$receiverWalletId)->decrement('balance', $this->transferAmount);
                DB::commit();
            }
        } catch (\Exception $e) {
            DB::rollback();
        }
    }
}

